#!/bin/bash

# script to make labels
# input 1 : odg file
# optional input 2 : number of columns (the number of rows adjst automtically to fit in an A4 portrait-mode). By default : 2
# optional input 3 : the final number of labels you want - if given the script will comput how much pages you should print
# output : the image to print and cut.

# param 2 : default is 2
nbx=${2:-2}

# approx final DPI 
dpi=200

# convert the odg to pdf
libreoffice --convert-to pdf "$1"
it=`basename -s .odg "$1"`.pdf
i=/tmp/o.png
# convert pdf to png with trim option
convert -density $(($dpi/$nbx)) "$it" -trim $i
# remove the PDF
rm $it

# compute how much we can fit in a page, given the ratio
w=`identify -format '%w' $i`
h=`identify -format '%h' $i`
# I'm counting 1cm margin in the page so dimensions are 277*190mm
nby=$(( $nbx*277 / ($h*190/$w) ))

# repeat an argument n times
var=$i
let nb=$(( $nbx * $nby - 1 ))
for ((x=0 ; x<$nb ; x++)); do 
  var+=" $i"
done

# make the montage with tiling
out=`basename -s .odg "$1"`.png
montage $var -tile $nbx\x$nby -geometry +1+1 $out

# display a report
echo "OK ; you have $(($nbx*$nby)) labels per page"

# optionally compute the necessary number of pages
if [ ! -z $3 ] 
then 
    echo "For $3 bottles, you should print $(($3/(($nbx*$nby+1)) +1 )) pages"
fi
