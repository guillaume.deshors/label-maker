Start from a label designed in LibreOffice Draw and specify how many columns you wish to fit in your A4 page (this will set the final width of the labels). You don't have to care about the white space around your label nor its dimensions or ratio. Then the script will:

1. convert it to PDF
2. convert the PDF to image and trim the white space around
3. find out its dimensions and compute how many labels it can fit vertically in the page given the number of columns
4. generate the final image by duplicating the label
5. if you specified how many labels you need in the end, it will tell you how many pages you should print !

The final image is to be printed with the option to fit in the page but without changing the ratio. Then you should of course cut out all the labels. To have guide for cutting it's useful to put a light grey frame around your label, this will create lines for cutting as there is no margin between the labels.  To glue them I use a simple mix of milk with a spoon of flour, its very effective yet simple to wash at the next brew. Here is an example output :

![example](IPA.png)

Work to do : add options to use paper of different size or orientation. Add option to specify DPI.

Dependencies : libreoffice, imagemagick (convert, montage)

### Usage:

* input 1 : odg file
* optional input 2 : number of columns (the number of rows adjst automtically to fit in an A4 portrait-mode). By default : 2
* optional input 3 : the number of bottles you want to label - if provided the script will comput how much pages you should print
* output : the image to print and cut.

### Example

<code sh>./label-maker.sh my-nice-label.odg 2 106</code>


